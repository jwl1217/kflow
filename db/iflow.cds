using {
    Currency,
    managed
} from '@sap/cds/common';

namespace ifl.iflow;


entity IFLT0001 : managed {
    key FLOWUUID                : UUID @odata.Type : 'Edm.String'; /*Mapping UUIDs to OData 참고*/
        FLOWCODE            : String(5);
        FLOWNO              : String(10);
        FLOWCNT             : String(3);
        BUKRS               : String(4);
        CREATOR_LOGIN_ID    : String(20);
        CREATOR_NAME        : String(35);
        CREATOR_PERNR       : String(10);
        CREATOR_ORGEH       : String(14);
        CREATOR_ORGTX       : String(40);
        CREATOR_RANK        : String(3);
        CREATOR_RANKT       : String(20);
        CREATOR_KOSTL       : String(10);
        CREATOR_KOSTLT      : String(20);
        TITLE               : String(100);
        CREATE_DATE         : Date;
        CREATE_TIME         : Time;
        END_DATE            : Date;
        END_TIME            : Time;
        APPR_CNT            : Decimal(3,0);
        CUR_CNT             : Decimal(3,0);
        APPR_STATUS         : String(1);
        WFEND               : Boolean;
        CUR_FLOWCNT         : String(3);
        DEADDAY             : Date;
        DEADTIME            : Time;
        SEPARATE            : String(1);
        SEPARATED_FLOW      : String(1);
        MAIN_FLOWCODE       : String(5);
        MAIN_FLOWNO         : String(10);
        MAIN_FLOWCNT        : String(3);
        ATTACH_UUID         : UUID;
        APPRHD              : Composition of many IFLT0002
                               on APPRHD.FLOWUUID = FLOWUUID;
        APPRIT              : Composition of many IFLT0003 
                               on APPRIT.FLOWUUID = FLOWUUID ;
        TEXT                : Composition of many IFLT0004
                               on TEXT.FLOWUUID = FLOWUUID ;
        ATTACH              : Composition of many IFLT0006
                               on ATTACH.ATTACH_UUID = ATTACH_UUID ;

        /* "Composition of"는 Deep CRUD가 가능하게 해줍니다. */
}

entity IFLT0002 : managed {
    key FLOWUUID        : UUID @odata.Type : 'Edm.String'; /*Mapping UUIDs to OData 참고*/
    key FLOWIT          : String(3);
        FLOWCODE        : String(5);
        FLOWNO          : String(10);
        FLOWCNT         : String(3);
        WFIT_TYPE       : String(2);
        CREATE_DATE     : Date;
        CREATE_TIME     : Time;
        START_DATE      : Date;
        START_TIME      : Time;
        END_DATE        : Date;
        END_TIME        : Time; 
        IT_WFSTAT       : String(1);
        IT_CNT          : Decimal(3,0);
        APPROVE_CNT     : Decimal(3,0);
        AGREE_TYPE      : String(1);
        
}
entity IFLT0003 : managed {
    key FLOWUUID        : UUID @odata.Type : 'Edm.String'; /*Mapping UUIDs to OData 참고*/
    key FLOWIT          : String(3);
    key FLOWIT_NO       : String(2);
        FLOWCODE        : String(5);
        FLOWNO          : String(10);
        FLOWCNT         : String(3);
        APPROVER_LOGIN_ID : String(20);
        APPROVER_NAME   : String(20);
        APPROVER_PERNR  : String(10);
        APPROVER_ORGEH  : String(14);
        APPROVER_ORGTX  : String(40);
        APPROVER_RANK   : String(3);
        APPROVER_RANKT  : String(20);
        APPROVER_KOSTL  : String(10);
        APPROVER_KOSTLT : String(20);
        SUB_LOGIN_ID : String(20);
        SUB_NAME   : String(20);
        SUB_PERNR  : String(10);
        SUB_ORGEH  : String(14);
        SUB_ORGTX  : String(40);
        SUB_RANK   : String(3);
        SUB_RANKT  : String(20);
        SUB_KOSTL  : String(10);
        SUB_KOSTLT : String(20);
        WFIT_TYPE       : String(2);
        WFIT_SUB_TYPE   : String(2);
        CREATE_DATE     : Date;
        CREATE_TIME     : Time;
        START_DATE      : Date;
        START_TIME      : Time;
        END_DATE        : Date;
        END_TIME        : Time; 
        DEAD_DATE       : Date;
        DEAD_TIME       : Time;
        WFSTAT          : String(1);
        AGREE_TYPE      : String(1);
        FIX             : Boolean;
        SUB_APPROVE     : Boolean;
}

entity IFLT0004 : managed {
    key FLOWUUID        : UUID @odata.Type : 'Edm.String'; /*Mapping UUIDs to OData 참고*/
    key TEXT_NO         : String(2);
        FLOWCODE        : String(5);
        FLOWNO          : String(10);
        FLOWCNT         : String(3); 
        APPROVER_LOGIN_ID: String(20);
        APPROVER_NAME   : String(35);
        APPROVER_ORGEH  : String(14);
        APPROVER_ORGTX  : String(40);
        APPROVER_RANK   : String(3);
        APPROVER_RANKT  : String(20);
        APPROVER_KOSTL  : String(10);
        APPROVER_KOSTLT : String(20);
        CREATE_DATE     : Date;
        CRATE_TIME      : Time;
        APPR_TEXT       : String(3000);
        
}

define type IFLS001 {
        FLOWCODE        : String(5);
        FLOWNO          : String(10);
        FLOWCNT         : String(3);
        BUKRS           : String(4);
        FLOWCODE_NAME   : String(50);
        TITLE           : String(80);
        WFIT_TYPE       : String(2);
        WFIT_SUB_TYPE   : String(2);
        WFSTAT          : String(1);
        APPROVER_NAME   : String(35);
        APPROVER_ORGEH  : String(14);
        APPROVER_ORGTX  : String(40);
        APPROVER_RANK   : String(3);
        APPROVER_RANKT  : String(20);
        APPROVER_KOSTL  : String(10);
        APPROVER_KOSTLT : String(20);
        CREATE_DATE     : Date;
        CRATE_TIME      : Time;
        END_DATE        : Date;
        END_TIME        : Time;
        SUB             : Boolean;
        SUB_APPROVE     : Boolean;
        WFEND           : Boolean;
        DEAD_DATE       : Date;
        DEAD_TIME       : Time;
}
entity IFLT0005 : managed {
    key APPROVER_LOGIN_ID : String(20);
    key FLOWUUID        : UUID @odata.Type : 'Edm.String'; 
    key FLOWIT          : String(3);
    key FLOWIT_NO       : String(2);    
        APPRDATA        : IFLS001;       
}
entity IFLT0005A  : managed {
    key APPROVER_LOGIN_ID : String(20);
    key FLOWUUID        : UUID @odata.Type : 'Edm.String'; 
    key FLOWIT          : String(3);
    key FLOWIT_NO       : String(2);    
        APPRDATA        : IFLS001;       
}

entity IFLT0005O  : managed {
    key APPROVER_LOGIN_ID : String(20);
    key FLOWUUID        : UUID @odata.Type : 'Edm.String'; 
    key FLOWIT          : String(3);
    key FLOWIT_NO       : String(2);    
        APPRDATA        : IFLS001;       
}

entity IFLT0006 : managed {
    key ATTACH_UUID        : UUID @odata.Type : 'Edm.String'; /*Mapping UUIDs to OData 참고*/
    key ATTACH_NO       : String(2);
        FLOWCODE        : String(5);
        FLOWNO          : String(10);
        FLOWCNT         : String(3); 
        ATTACH_LOGIN_ID : String(20);
        ATTACH_NAME     : String(35);
        ATTACH_ORGEH    : String(14);
        ATTACH_ORGTX    : String(40);
        ATTACH_RANK     : String(3);
        ATTACH_RANKT    : String(20);
        ATTACH_KOSTL    : String(10);
        ATTACH_KOSTLT   : String(20);
        ATTACH_DATE     : Date;
        ATTACH_TIME     : Time;
        ATTACH_TYPE     : String(1);
        FILE_NAME       : String(60);
        FILE_PATH       : String(300);
        MIME_TYPE       : String(100);
        FILE_EXTENTION  : String(10);
        FILE_DESCRIPTION : String(50);
        FILEDATA      : LargeBinary;
}




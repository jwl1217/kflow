using {
    managed
} from '@sap/cds/common';

namespace ifl.config;
// 결재 항목 코드 
entity IFLTC001 : managed {
    key FLOWCODE         : String(5);
        FLOWCODE_T       : localized String(50);
        FLOW_GROUP       : String(2);
        EXE_CODE         : String(100);
        TOTAL_APPROVE    : String(100);      
}
// 결재 항목 코드 내역 
define entity IFLTC001_texts @(cds.autoexpose) {
    key locale      : String(5);
    key FLOWCODE    : String(5);
        FLOWCODE_T  : String(50);
}
// 결재항목 코드 + 코드내역 
extend entity IFLTC001 with {
    texts     : Composition of many IFLTC001_texts
                    on texts.FLOWCODE = FLOWCODE;
    localized : Association to IFLTC001_texts
                    on  localized.FLOWCODE  = FLOWCODE
                    and localized.locale = $user.locale;
}
//*****************************************************************/
//사원 마스터 
entity  IFLTC002   : managed{
    key LOGIN_ID  : String(20);
    key ENDDA     : Date;
        BEGDA     : Date;
        BUKRS     : String(4);
        NAME      : String(40);
        ENG_NAME  : String(40);
        PERNR     : String(10);
        ORGEH     : String(20);
        ORGTX     : String(40);
        RANK      : String(3);
        RANK_NAME : String(40);
        POSITION  : String(3);
        POSITION_NAME : String(40);
        KOSTL     : String(10);
        KOSTL_NAME : String(40);
        MAIL      : String(50);
        PHONE     : String(20);
        PASS      : String(50);
        CAP_LOGIN_ID : String(20);
        LOCK_MODE : Boolean;
} 
//*****************************************************************/

//조직 마스터 
entity  IFLTC003 : managed {
    key BUKRS   : String(4);
    key ORGEH   : String(14);
    key ENDDA   : Date;  
    ORGEH_T     : localized String(50);
    BEGDA       : Date;
    P_ORGEH     : String(14);
    HD_LOGIN_ID : String(20);
    KOSTL       : String(10);
    SORT_CNT    : Decimal(3,0);
    LTEXT       : String(40);
}
// 조직 이름 
define entity  IFLTC003_texts @(cds.autoexpose) {
    key locale  : String(5);
    key BUKRS   : String(4);
    key ORGEH   : String(14);
    key ENDDA   : Date;  
        ORGEH_T   : String(40);  
}
// 조직 코드 + 코드내역 
extend entity IFLTC003 with {
    texts     : Composition of many IFLTC003_texts
                    on texts.BUKRS = BUKRS and
                       texts.ORGEH = ORGEH and
                       texts.ENDDA = ENDDA;
    localized : Association to IFLTC003_texts
                    on  localized.BUKRS  = BUKRS
                    and localized.ORGEH  = ORGEH
                    and localized.locale = $user.locale;
}
//*****************************************************************/

// 직급
entity  IFLTC007    : managed{
    key BUKRS       : String(4);
    key RANK        : String(3);
        RANK_T      : localized String(20);
}
// 직급 이름 
define entity  IFLTC007_texts @(cds.autoexpose) {
    key locale      : String(5);
    key BUKRS       : String(4);
    key RANK        : String(3);
        RANK_T      : String(20);
}
// 직급 코드 + 내역
extend entity IFLTC007 with {
    texts     : Composition of many IFLTC007_texts
                    on texts.BUKRS = BUKRS and
                       texts.RANK = RANK;
    localized : Association to IFLTC007_texts
                    on  localized.BUKRS = BUKRS 
                    and localized.RANK = RANK
                    and localized.locale = $user.locale;
}
//*****************************************************************/
// 결재프로세스별 Activity관리
entity  IFLTC014    : managed{
    key BUKRS       : String(4);
    key FLOW_GROUP  : String(2);
    key FLOWCODE    : String(5);
    key WFIT_TYPE   : String(2);
    key WFIT_SUB_TYPE : String(2);
    key WFSTAT      : String(1);
    key ACTIVITY    : String(20);
    key WFEND       : Boolean;
        SORT_CNT    : Decimal(2,0);
        HIDE        : Boolean;
        ICON        : String(50);
        ACTIVITY_T  : localized String(20);
}
// 결재프로세스별 Activity 이름  
define entity  IFLTC014_texts @(cds.autoexpose) {
    key locale      : String(5);
    key BUKRS       : String(4);
    key FLOW_GROUP  : String(2);
    key FLOWCODE    : String(5);
    key WFIT_TYPE   : String(2);
    key WFIT_SUB_TYPE : String(2);
    key WFSTAT      : String(1);
    key ACTIVITY    : String(20);
    key WFEND       : Boolean;
        ACTIVITY_T  : String(20);
}
// 결재프로세스별 Activity + 이름 
extend entity IFLTC014 with {
    texts     : Composition of many IFLTC014_texts
                    on texts.BUKRS = BUKRS and
                       texts.FLOW_GROUP = FLOW_GROUP and 
                       texts.FLOWCODE = FLOWCODE and
                       texts.WFIT_TYPE = WFIT_TYPE and
                       texts.WFIT_SUB_TYPE = WFIT_SUB_TYPE and
                       texts.WFSTAT = WFSTAT and
                       texts.ACTIVITY = ACTIVITY and
                       texts.WFEND = WFEND ;
    localized : Association to IFLTC014_texts
                    on localized.BUKRS = BUKRS and
                       localized.FLOW_GROUP = FLOW_GROUP and 
                       localized.FLOWCODE = FLOWCODE and
                       localized.WFIT_TYPE = WFIT_TYPE and
                       localized.WFIT_SUB_TYPE = WFIT_SUB_TYPE and
                       localized.WFSTAT = WFSTAT and
                       localized.ACTIVITY = ACTIVITY and
                       localized.WFEND = WFEND and
                       localized.locale = $user.locale;
}
//*****************************************************************/

// Activity
entity  IFLTC019    : managed{
    key ACTIVITY    : String(20);
        LTEXT  : String(20);
}
//*****************************************************************/ 

// 첨부 파일 유형
entity  IFLTC020    : managed{
    key FILE_TYPE   : String(2);
        ATTACH_TYPE : String(1);
        FILE_TYPE_T      : localized String(40);
}



define entity  IFLTC020_texts @(cds.autoexpose) {
    key locale      : String(5);
    key FILE_TYPE   : String(2);
        FILE_TYPE_T : String(40);
}

// 직급 코드 + 내역
extend entity IFLTC020 with {
    texts     : Composition of many IFLTC020_texts
                    on texts.FILE_TYPE = FILE_TYPE;
    localized : Association to IFLTC020_texts
                    on  localized.FILE_TYPE = FILE_TYPE 
                    and localized.locale = $user.locale;
}
//*****************************************************************/

// 결재 프로세스별 첨부 문서 지정
entity  IFLTC036    : managed{
    key BUKRS       : String(4);
    key FLOW_GROUP  : String(2);
    key FLOWCODE    : String(5);
    key FILE_TYPE   : String(2);
        SORT_CNT    : Decimal(2,0);
        REQUIRED    : Boolean;
}
//*****************************************************************/
// 결재항목 그룹 
entity  IFLTC048    : managed{
    key FLOW_GROUP      : String(2);
        FLOW_GROUP_T    : localized String(40);
}
// 결재항목 그룹 이름 
define entity  IFLTC048_texts @(cds.autoexpose) {
    key locale          : String(5);
    key FLOW_GROUP      : String(2);
        FLOW_GROUP_T    : String(40);
}

// 결재항목 그룹 코드 + 결재항목 그룹내역 
extend entity IFLTC048 with {
    texts     : Composition of many IFLTC048_texts
                    on texts.FLOW_GROUP = FLOW_GROUP;
    localized : Association to IFLTC048_texts
                    on  localized.FLOW_GROUP = FLOW_GROUP
                    and localized.locale = $user.locale;
}
//*****************************************************************/
// 결재선 Header
entity  IFLTC061    : managed{
    key BUKRS       : String(4);
    key FLOW_GROUP  : String(2);
    key FLOWCODE    : String(5);
    key APPRLINE    : String(10);
        IN_LEVEL    : Decimal(2,0);
        REQ_LEVEL   : Decimal(2,0);
        APPRLINE_T  : localized String(80);
    DETAIL          : Composition of many IFLTC062
                               on DETAIL.BUKRS = BUKRS and
                                  DETAIL.FLOW_GROUP  = FLOW_GROUP and
                                  DETAIL.FLOWCODE = FLOWCODE and
                                  DETAIL.APPRLINE = APPRLINE;
}
// 결재선 내역 지정 
define entity  IFLTC061_texts @(cds.autoexpose) {
    key locale      : String(5);
    key BUKRS       : String(4);
    key FLOW_GROUP  : String(2);
    key FLOWCODE    : String(5);
    key APPRLINE    : String(10);
        APPRLINE_T  : String(80);
}
// 결재선 코드 + 결재선 내역
extend entity IFLTC061 with {
    texts     : Composition of many IFLTC061_texts
                    on texts.BUKRS = BUKRS and
                       texts.FLOW_GROUP = FLOW_GROUP and 
                       texts.FLOWCODE = FLOWCODE and
                       texts.APPRLINE = APPRLINE;
    localized : Association to IFLTC061_texts
                    on localized.BUKRS = BUKRS and
                       localized.FLOW_GROUP = FLOW_GROUP and 
                       localized.FLOWCODE = FLOWCODE and
                       localized.APPRLINE = APPRLINE;
}
//결재선별 결재자 지정 
entity  IFLTC062    : managed{
    key BUKRS       : String(4);
    key FLOW_GROUP  : String(2);
    key FLOWCODE    : String(5);
    key APPRLINE    : String(10);
    key CNT         : Decimal(2,0);
    key A_CNT       : Decimal(2,0);
        APPR_TYPE   : String(2);
        ROLE        : String(2);
        ORGEH       : String(14);
        LOGIN_ID    : String(20);
        AGREE_TYPE  : String(1);
        FROM_AMT    : Decimal(15,2);
        TO_AMT      : Decimal(15,2);
}

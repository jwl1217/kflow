using {ifl.config as db} from '../db/config';

service iflowConfig @(path : 'IFL_CON') {
    entity IFLTC001 as projection on db.IFLTC001;
    entity IFLTC002 as projection on db.IFLTC002;
    entity IFLTC003 as projection on db.IFLTC003;
    entity IFLTC007 as projection on db.IFLTC007;
    entity IFLTC014 as projection on db.IFLTC014;
    entity IFLTC019 as projection on db.IFLTC019;
    entity IFLTC020 as projection on db.IFLTC020;
    entity IFLTC036 as projection on db.IFLTC036;
    entity IFLTC048 as projection on db.IFLTC048;
    entity IFLTC061 as projection on db.IFLTC061;
    entity IFLTC062 as projection on db.IFLTC062; 
}   


using {ifl.iflow as dbt} from '../db/iflow';

service iflowService @(path : 'IFL_IFLOW') {
    entity IFLT0001 as projection on dbt.IFLT0001;
    entity IFLT0002 as projection on dbt.IFLT0002;
    entity IFLT0003 as projection on dbt.IFLT0003;
    entity IFLT0004 as projection on dbt.IFLT0004;
    entity IFLT0005 as projection on dbt.IFLT0005;
    entity IFLT0006 as projection on dbt.IFLT0006;
}